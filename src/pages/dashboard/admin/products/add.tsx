import { ProductAdd } from "@/features/products/add/components/product-add";
import React from "react";

export default function add() {
  return (
    <div>
      <ProductAdd />
    </div>
  );
}
