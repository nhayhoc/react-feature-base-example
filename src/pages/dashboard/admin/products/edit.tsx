import { ProductEdit } from "@/features/products/edit/components/product-edit";
import React from "react";

export default function edit() {
  return (
    <div>
      <ProductEdit />
    </div>
  );
}
