import Image from "next/image";
import { Inter } from "next/font/google";
import { Link } from "lucide-react";
import NextLink from "next/link";
const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <main>
      <NextLink
        href="/dashboard/admin/products/add"
        className="underline text-blue-500 block"
      >
        Add Product
      </NextLink>
      <NextLink
        href="/dashboard/admin/products/edit?id=abc"
        className="underline text-blue-500 block"
      >
        Edit Product
      </NextLink>
    </main>
  );
}
