import { useForm } from "react-hook-form";
import {
  ProductFormValues,
  ProductFormValuesSchema,
} from "./product-form-type";
import { zodResolver } from "@hookform/resolvers/zod";

export const useProductForm = (args?: {
  defaultValues?: ProductFormValues | (() => Promise<ProductFormValues>);
}) => {
  const form = useForm<ProductFormValues>({
    defaultValues: args?.defaultValues,
    resolver: zodResolver(ProductFormValuesSchema),
  });

  return form;
};
