import React from "react";
import { ProductFormValues } from "./product-form-type";

import { Button } from "@/components/ui/button";
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { UseFormReturn } from "react-hook-form";
import { NumberWithCommas } from "@/lib/NumberWithCommas";
import RemoveUnicodeLowerCase from "@/lib/RemoveUnicodeLowerCase";

export default function ProductForm({
  form,
  handleSubmit,
  isMutating,
}: {
  form: UseFormReturn<ProductFormValues>;
  handleSubmit: React.FormEventHandler<HTMLFormElement>;
  isMutating: boolean;
}) {
  return (
    <Form {...form}>
      <form
        onSubmit={handleSubmit}
        className="w-full max-w-[500px] mx-auto mt-10"
      >
        <FormField
          control={form.control}
          name="slug"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Slug</FormLabel>
              <FormControl>
                <Input
                  {...field}
                  onChange={(e) => {
                    const value = e.target.value;
                    field.onChange(
                      RemoveUnicodeLowerCase(value || "").replace(
                        /[^a-z0-9_]+/g,
                        "-"
                      )
                    );
                  }}
                />
              </FormControl>
              <FormDescription>Slug is used for product url</FormDescription>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Name</FormLabel>
              <FormControl>
                <Input {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="price"
          render={({ field }) => (
            <FormItem>
              <FormLabel>Price</FormLabel>
              <FormControl>
                <Input
                  {...field}
                  onChange={(e) => {
                    const value = e.target.value;
                    field.onChange(Number(value.replace(/,/g, "")));
                  }}
                  value={NumberWithCommas(field.value || 0)}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Button type="submit" disabled={isMutating} className="mt-4">
          {isMutating ? "Saving..." : "Save"}
        </Button>
      </form>
    </Form>
  );
}
