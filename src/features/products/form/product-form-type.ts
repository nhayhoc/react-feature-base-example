import { PreprocessInputNumberWithCommas } from "@/lib/preprocessInputNumberFormatCurrency";
import { z } from "zod";

export const ProductFormValuesSchema = z.object({
  slug: z.string().regex(/^[a-zA-Z0-9_-]*$/),
  name: z.string(),
  price: z.preprocess(PreprocessInputNumberWithCommas, z.number().min(0)),
});

export type ProductFormValues = z.infer<typeof ProductFormValuesSchema>;
