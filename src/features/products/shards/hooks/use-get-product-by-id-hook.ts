import { useState } from "react";
import {
  ProductDTO,
  getProductByIdFetcher,
  productUrl,
} from "../apis/get-product-by-id.api";

// swr not support lazy fetcher yet, so i use this workaround
export const useGetProductByIdHook = (id: string) => {
  const [data, setData] = useState<ProductDTO | undefined>(undefined);
  const [error, setError] = useState<any>(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const trigger = async (): Promise<ProductDTO> => {
    setIsLoading(true);
    setError(undefined);
    setData(undefined);
    try {
      const data = await getProductByIdFetcher(`${productUrl}/${id}`);
      setData(data);
      setIsLoading(false);
      return data;
    } catch (error) {
      setIsLoading(false);
      setError(error);
      return {
        id,
        name: "",
        price: 0,
        slug: "",
      };
    }
  };

  return {
    data,
    error,
    isError: !!error,
    isLoading,
    trigger,
  };
};
