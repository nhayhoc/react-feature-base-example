import { sleep } from "@/lib/sleep";

export type ProductDTO = {
  id: string;
  slug: string;
  name: string;
  price: number;
};

export const productUrl = "/api/products";
export const getProductByIdFetcher = async (_: string): Promise<ProductDTO> => {
  // Simulate network latency and return a fake product
  await sleep(1000);
  return {
    id: "123",
    slug: "slug",
    name: "name",
    price: 100,
  };
};
