import { sleep } from "@/lib/sleep";

export type UpdateProductBodyDTO = {
  slug: string;
  name: string;
  price: number;
};
export type UpdateProductResDTO = {
  id: string;
  slug: string;
  name: string;
  price: number;
};
export const UpdateProductUrl = "/api/products";
export const UpdateProductFetcher = async (
  url: string,
  { arg }: { arg: UpdateProductBodyDTO }
): Promise<UpdateProductResDTO> => {
  // Simulate network latency and return a fake product
  await sleep(1000);
  return {
    id: "123",
    slug: arg.slug,
    name: arg.name,
    price: arg.price,
  };
};
