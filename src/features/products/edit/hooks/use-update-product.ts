import { useSWRConfig } from "swr";
import useSWRMutation from "swr/mutation";
import {
  UpdateProductFetcher,
  UpdateProductUrl,
} from "../apis/update-product.api";
import { useGetProductByIdHook } from "../../shards/hooks/use-get-product-by-id-hook";
import { useProductForm } from "../../form/product-form-hook";
import { useRouter } from "next/router";
import setFormFieldError from "@/lib/setFormFieldError";

export function useUpdateProduct() {
  const { mutate } = useSWRConfig();

  const router = useRouter();
  // warn: id is string | string[] | undefined => please check type before use
  const id = router.query.id as string;

  const {
    isLoading: isInitProductLoading,
    error: initProductError,
    isError: isInitProductError,
    trigger: initProductTrigger,
  } = useGetProductByIdHook(id);

  const form = useProductForm({
    defaultValues: async () => {
      const product = await initProductTrigger();
      // transform (if need): ProductDTO => ProductFormValues
      return product;
    },
  });

  const { isMutating, trigger } = useSWRMutation(
    `${UpdateProductUrl}/${id}`,
    UpdateProductFetcher,
    {
      onError: (error) => {
        // when server throw error, we set error to form
        setFormFieldError(form.setError, error);
        if (error.status == 409) {
          form.setError("slug", {
            type: "manual",
            message: "Slug already exists",
          });
        }
      },
      onSuccess: (data) => {
        mutate("/api/products");
        mutate(`/api/products${id}`);
        mutate(`/api/products${data.slug}`);

        // replace this with toast
        alert("Product updated: " + JSON.stringify(data));
      },
    }
  );

  const handleSubmit = form.handleSubmit((data) => {
    // transform (if need): ProductFormValues => CreateProductBodyDTO
    trigger(data);
  });

  return {
    id,

    isInitProductLoading,
    initProductError,
    isInitProductError,

    form,
    handleSubmit,

    isMutating,
  };
}
