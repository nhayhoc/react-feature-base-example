import ProductForm from "../../form/product-form";
import { useUpdateProduct } from "../hooks/use-update-product";

export function ProductEdit() {
  const {
    id,

    isInitProductLoading,
    initProductError,
    isInitProductError,

    form,
    handleSubmit,

    isMutating,
  } = useUpdateProduct();
  return (
    <div>
      <h1 className="text-center">Product Edit {id}</h1>
      {isInitProductLoading && <div className="text-center">Loading...</div>}
      {isInitProductError && (
        <div className="text-center">{initProductError.message}</div>
      )}
      <ProductForm
        handleSubmit={handleSubmit}
        form={form}
        isMutating={isMutating}
      />
    </div>
  );
}
