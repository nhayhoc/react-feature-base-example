import ProductForm from "../../form/product-form";
import { useProductAdd } from "../hooks/use-product-add";

export function ProductAdd() {
  const { form, isMutating, handleSubmit } = useProductAdd();
  return (
    <div>
      <h1>Product Add</h1>
      <ProductForm
        handleSubmit={handleSubmit}
        form={form}
        isMutating={isMutating}
      />
    </div>
  );
}
