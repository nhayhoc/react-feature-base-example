import { sleep } from "@/lib/sleep";

export type CreateProductBodyDTO = {
  slug: string;
  name: string;
  price: number;
};
export type CreateProductResDTO = {
  id: string;
  slug: string;
  name: string;
  price: number;
};
export const CreateProductUrl = "/api/products";
export const CreateProductFetcher = async (
  url: string,
  { arg }: { arg: CreateProductBodyDTO }
): Promise<CreateProductResDTO> => {
  // Simulate network latency and return a fake product
  await sleep(1000);
  return {
    id: "123",
    slug: arg.slug,
    name: arg.name,
    price: arg.price,
  };
};
