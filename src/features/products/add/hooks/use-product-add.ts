import setFormFieldError from "@/lib/setFormFieldError";
import { useProductForm } from "../../form/product-form-hook";
import useSWRMutation from "swr/mutation";
import { useSWRConfig } from "swr";
import {
  CreateProductFetcher,
  CreateProductUrl,
} from "../apis/product-add.api";
export function useProductAdd() {
  const form = useProductForm();
  const { mutate } = useSWRConfig();

  const { isMutating, trigger } = useSWRMutation(
    CreateProductUrl,
    CreateProductFetcher,
    {
      onSuccess: (data) => {
        mutate("/api/products");
        alert("Product created: " + JSON.stringify(data));
      },
      onError: (error) => {
        setFormFieldError(form.setError, error);
        if (error.status == 409) {
          form.setError("slug", {
            type: "manual",
            message: "Slug already exists",
          });
        }
      },
    }
  );

  const handleSubmit = form.handleSubmit((data) => {
    // transform (if need): ProductFormValues => CreateProductBodyDTO
    trigger(data);
  });

  return { form, isMutating, handleSubmit };
}
